/*global module:false*/
module.exports = function(grunt) {
    'use strict';

    grunt.initConfig({
        sass: {
            dev: {
                files: {
                    'css/style.css': 'scss/style.scss'
                },
                options: {
                    style: 'expanded',
                    debugInfo: true
                }
            },
            prod: {
                files: {
                    'css/style.build.css': 'scss/style.scss'
                },
                options: {
                    style: 'compressed'
                }
            }
        },


        sprite:{
            all: {
                src: 'i/ico/*.png',
                destImg: 'i/sprite.png',
                destCSS: 'scss/sprite.scss',
                sort: 'filename'
            }
        },

        watch: {
            sass: {
                files: ['scss/*.scss'],
                tasks: 'sass'
            }
        },

        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'i/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'i/'
                }]
            }
        }
    });

    // Подключаем плагины
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-spritesmith');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
};